<?php
//ob_start();

defined('BASEPATH') OR exit('No direct script access allowed');

//require __DIR__.'/vendor/autoload.php';

use \Spipu\Html2Pdf\Html2Pdf;
use \OTPHP\TOTP;


class HoaDon extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $arrayLibraries = [
            'benban_library',
            'benmua_library',
            'sanpham_library',
            'hinhthucthanhtoan_library',
            'hoadon_library',
            'helper_library',
            'connect_library'
        ];
        $this->load->library($arrayLibraries);
    }

    public function index()
    {
//        $this->load->helper('url');
//        $this->load->view('hoadon');

        echo "123123";
//

        $totp = new \OTPHP\TOTP("base32secret3232");
        $totp->now();

        $html2pdf = new Html2Pdf();
        $html = '
        <img src="http://cdn.shopify.com/s/files/1/0257/6087/products/Pikachu_Single_Front_dc998741-c845-43a8-91c9-c1c97bec17a4.png?v=1523938908" width="150">
        <img src="https://www.cognex.com/BarcodeGenerator/Content/images/isbn.png" width="150">
        <p class="p-1" style="font-size: 50px; font-style: italic; text-decoration: line-through;">This is my content.</p>
        <div class="wrapper" id="wrapper-1">
            <p class="p-1">This is my content.</p>
            <h1>This is my H1.</h1>
            <strong>STRONG</strong>
            <ul>
                <li>1</li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li></li>
            </ul>
            <ol>
                <li>1</li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li></li>
            </ol>
        </div>
        ';
        try{
            $html2pdf->writeHTML($html);
            ob_end_clean();
            $result = $html2pdf->output();
            return $result;
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }

    public function create(){
        $benBan = new BenBan_Library();
        $benBan->setBenBanMaDonVi("123");
        $benBan->setBenBanTenDonVi("CÔNG TY CỔ PHẦN BỆNH VIỆN PHỤ SẢN QUỐC TẾ SÀI GÒN");
        $benBan->setBenBanMaSoThue("0301482886");
        $benBan->setBenBanDiaChi("63 Bùi Thị Xuân, P.Phạm Ngũ Lão, Quận 1, TP.HCM");

        $benMua = new BenMua_Library();
        //$benMua->setBenMuaMaDonVi("");
        $benMua->setBenMuaMaSoThue("0101300842");
        $benMua->setBenMuaTenDonVi("Công ty phát triển công nghệ thái sơn");
        $benMua->setBenMuaHoTen("vu tho tuyền");
        $benMua->setBenMuaDiaChi("Số 99B tổ 70 Hồ Quỳnh, Phường Thanh Nhàn, Quận Hai Bà Trưng, Hà Nội");
        //$benMua->setBenMuaDienThoai("");
        //$benMua->setBenMuaFax("");
        //$benMua->setBenMuaEmail("");
        $benMua->setBenMuaTaiKhoanNganHang("0308808576");
        $benMua->setBenMuaTenNganHang("Ngân hàng Quân đội");

        $sanPham = new SanPham_Library();
        $sanPham->setSoThuTu("1");
        $sanPham->setMaHang("");
        $sanPham->setTenHang("Máy tính Casio");
        $sanPham->setDonViTinh("cái");
        $sanPham->setSoLuong("1");
        $sanPham->setDonGia("500000");
        $sanPham->setThanhTien("500000");
        $sanPham->setVat("10");
        $sanPham->setTienVat("50000");

        $hinhThucThanhToan = new HinhThucThanhToan_Library();
        $hinhThucThanhToan->setHinhThucThanhToan("01");
        $hinhThucThanhToan->setTamUng("0");
        $hinhThucThanhToan->setTienChietKhau("0");
        $hinhThucThanhToan->setTyGia("1");

        $hoaDon = HoaDon_Library::XuatHoaDonDienTu(array($sanPham), $benBan, $benMua, $hinhThucThanhToan);
        return $hoaDon;
    }

    public function cancel(){
        $hoaDon = new HoaDon_Library();
        $hoaDon->setUserHuy("tuyenvt");
        $hoaDon->setSoHoaDon("0000017");
        $hoaDon->setNgayTaoHoaDon("2016-11-25");
        $hoaDon->setNoiDung("khách hàng không sử dụng dịch vụ");
        $result = HoaDon_Library::HuyHoaDon($hoaDon);
        return $result;
    }

    public function update(){
        $hoaDonGoc = new HoaDon_Library();
        $hoaDonGoc->setMaLoaiHoaDon('01');
        $hoaDonGoc->setHoaDonId('22239');
        $hoaDonGoc->setSoHoaDon('0000048');
        $hoaDonGoc->setNgayNhapVien('0001-01-01');
        $hoaDonGoc->setNgayTaoHoaDon('2018-11-15');
        $hoaDonGoc->setNgayXuatHoaDon('2018-11-15');
        $hoaDonGoc->setMaEinvoice('883bc80c-661f-4f4b-82e8-90f0f5a2587d');
        $hoaDonGoc->setIsGiuLai('false');
        $hoaDonGoc->setIsSuDungBangKe('false');

        $benBan = new BenBan_Library();
        $benBan->setBenBanMaDonVi("123");
        $benBan->setBenBanTenDonVi("CÔNG TY CỔ PHẦN BỆNH VIỆN PHỤ SẢN QUỐC TẾ SÀI GÒN");
        $benBan->setBenBanMaSoThue("0301482886");
        $benBan->setBenBanDiaChi("63 Bùi Thị Xuân, P.Phạm Ngũ Lão, Quận 1, TP.HCM");

        $benMua = new BenMua_Library();
        $benMua->setBenMuaMaSoThue("0101300842");
        $benMua->setBenMuaTenDonVi("Công ty phát triển công nghệ thái sơn");
        $benMua->setBenMuaHoTen("vu tho tuyền");
        $benMua->setBenMuaDiaChi("Số 99B tổ 70 Hồ Quỳnh, Phường Thanh Nhàn, Quận Hai Bà Trưng, Hà Nội");
        $benMua->setBenMuaTaiKhoanNganHang("0308808576");
        $benMua->setBenMuaTenNganHang("Ngân hàng Quân đội");

        $sanPham = new SanPham_Library();
        $sanPham->setSoThuTu("1");
        $sanPham->setMaHang("");
        $sanPham->setTenHang("Máy tính Casio");
        $sanPham->setDonViTinh("cái");
        $sanPham->setSoLuong("1");
        $sanPham->setDonGia("500000");
        $sanPham->setThanhTien("500000");
        $sanPham->setVat("10");
        $sanPham->setTienVat("50000");

        $hinhThucThanhToan = new HinhThucThanhToan_Library();
        $hinhThucThanhToan->setHinhThucThanhToan("01");
        $hinhThucThanhToan->setTamUng("0");
        $hinhThucThanhToan->setTienChietKhau("0");
        $hinhThucThanhToan->setTyGia("1");

        $hoadonNew = HoaDon_Library::UpdateHoaDon($hoaDonGoc, array($sanPham), $benBan, $benMua, $hinhThucThanhToan);

        return $hoadonNew;
    }

    public function view(){
        $hoadonTemp = new HoaDon_Library();
        $hoadonTemp->setMaEinvoice("c1722933-e903-46cd-b8c8-f7c71a5cb25a&iscd=0");
        $linkView = HoaDon_Library::ViewHoaDon($hoadonTemp);
        return $linkView;
    }

    public function download(){
        $hoadonTemp = new HoaDon_Library();
        $hoadonTemp->setMaEinvoice("c1722933-e903-46cd-b8c8-f7c71a5cb25a&iscd=0");
        $linkView = HoaDon_Library::ViewHoaDon($hoadonTemp);
        $file = HoaDon_Library::DownloadHoaDon($linkView);
        return $file;
    }
}
