<?php

class Image extends CI_Controller {

    const FOLDER_IMAGE = FCPATH.'Origin/';

    public function __construct()
    {
        parent::__construct();
    }

    public function save(){
        $file = $_FILES['image'];
        $file_name = md5($file['name']);
        $destination = self::FOLDER_IMAGE.$file_name;
        $filename = $file['tmp_name'];
        try{
            $this->checkDirectory(self::FOLDER_IMAGE,true);
            move_uploaded_file($filename, $destination);

            $check = $this->checkFileExists($file_name);
            if($check){
                $this->load->library('image_library');
                $image = new Image_Library();
                $image->setId($file_name);
                $result =  [
                    'status' => [
                        'return_code' => '200',
                        'messenger' => 'OK'
                    ],
                    'image' => [
                        'id' => $image->getId()
                    ]
                ];
            }else{
                $result = [
                    'status' => [
                        'return_code' => '400',
                        'messenger' => 'ERROR'
                    ],
                    'image' => [
                        'id' => ''
                    ]
                ];
            }

        }catch (\Exception $exception){
            $result = [
                'status' => [
                    'return_code' => '400',
                    'messenger' => $exception->getMessage()
                ],
                'image' => [
                    'id' => ''
                ]
            ];
        }

        return json_encode($result);
    }

    /**
     * @param $imageId
     * @return string
     */
    public function get($imageId){
        $check = $this->checkFileExists($imageId);
        if($check){
            $result =  [
                'image' => self::FOLDER_IMAGE.$imageId
            ];
        }else{
            $result = [
                'image' => ''
            ];
        }

        return json_encode($result);
    }

    /**
     * @param $directory
     * @param bool $flag
     * @return bool
     */
    public function checkDirectory($directory, $flag = false){
        if(!is_dir($directory)) {
            if($flag){
                mkdir($directory, 0777, TRUE);
                return true;
            }
            return false;
        }else{
            return true;
        }
    }

    /**
     * @param $fileName
     * @return bool
     */
    public function checkFileExists($fileName){
        if(file_exists(self::FOLDER_IMAGE.$fileName)){
            return true;
        }else{
            return false;
        }
    }
}