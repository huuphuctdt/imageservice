<?php if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class HinhThucThanhToan_Library{
    public $tamUng;
    public $tyGia;
    public $tienChietKhau;
    public $hinhThucThanhToan;

    /**
     * HinhThucThanhToan constructor.
     * @param $tamUng
     * @param $tyGia
     * @param $hinhThucThanhToan
     */
    public function __construct($tamUng = '', $tyGia = '', $tienChietKhau = '', $hinhThucThanhToan = '')
    {
        $this->tamUng = $tamUng;
        $this->tyGia = $tyGia;
        $this->tienChietKhau = $tienChietKhau;
        $this->hinhThucThanhToan = $hinhThucThanhToan;
    }

    /**
     * @return mixed
     */
    public function getTamUng()
    {
        return $this->tamUng;
    }

    /**
     * @param mixed $tamUng
     */
    public function setTamUng($tamUng)
    {
        $this->tamUng = $tamUng;
    }

    /**
     * @return mixed
     */
    public function getTyGia()
    {
        return $this->tyGia;
    }

    /**
     * @param mixed $tyGia
     */
    public function setTyGia($tyGia)
    {
        $this->tyGia = $tyGia;
    }

    /**
     * @return mixed
     */
    public function getTienChietKhau()
    {
        return $this->tienChietKhau;
    }

    /**
     * @param mixed $tienChietKhau
     */
    public function setTienChietKhau($tienChietKhau)
    {
        $this->tienChietKhau = $tienChietKhau;
    }

    /**
     * @return mixed
     */
    public function getHinhThucThanhToan()
    {
        return $this->hinhThucThanhToan;
    }

    /**
     * @param mixed $hinhThucThanhToan
     */
    public function setHinhThucThanhToan($hinhThucThanhToan)
    {
        $this->hinhThucThanhToan = $hinhThucThanhToan;
    }

    /**
     * @param $hinhThucThanhToan
     * @return string
     */
    public static function formatXml($hinhThucThanhToan){
        $xml = "";
        if($hinhThucThanhToan instanceof HinhThucThanhToan_Library){
            $xml = '<inv:HinhThucThanhToan>'.$hinhThucThanhToan->getHinhThucThanhToan().'</inv:HinhThucThanhToan>
                    <inv:TyGia>'.$hinhThucThanhToan->getTyGia().'</inv:TyGia>
                    <inv:TamUng>'.$hinhThucThanhToan->getTamUng().'</inv:TamUng>
                    <inv:TienChietKhau>'.$hinhThucThanhToan->getTienChietKhau().'</inv:TienChietKhau>';
        }
        return $xml;
    }
}