<?php if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class HoaDon_Library
{

    const TenLoaiHoaDon = "Hóa đơn giá trị gia tăng";
    const MauSo = "01GTKT0/001";
    const KyHieu = "AA/18E";

    public static $KEY_XML_FUNCTION_XUAT_HOA_DON_DIEN_TU = "XuatHoaDonDienTu";
    public static $KEY_XML_FUNCTION_HUY_HOA_DON = "HuyHoaDon";
    public static $KEY_XML_RESPONSE_XUAT_HOA_DON_DIEN_TU= "XuatHoaDonDienTuResponse";
    public static $KEY_XML_RESULT_XUAT_HOA_DON_DIEN_TU = "XuatHoaDonDienTuResult";
    public static $KEY_XML_RESPONSE_HUY_HOA_DON_DIEN_TU = "HuyHoaDonResponse";
    public static $KEY_XML_RESULT_HUY_HOA_DON_DIEN_TU = "HuyHoaDonResult";

    const linkViewHoaDon = "http://210.245.8.58:6789/HoaDonPDF.aspx?mhd=";
    const startXuatHoaDonXml = "<inv:hoaDonEntity>";
    const endXuatHoaDonXml = "</inv:hoaDonEntity>";

    const startHuyHoaDonXml = "<inv:hoaDonHuy>";
    const endHuyHoaDonXml = "</inv:hoaDonHuy>";

    public $userHuy;
    public $soHoaDon;
    public $ngayTaoHoaDon;
    public $noiDung;
    public $maLoaiHoaDon;
    public $hoaDonId;
    public $ngayNhapVien;
    public $ngayXuatHoaDon;
    public $maEinvoice;
    public $isSuDungBangKe;
    public $isGiuLai;

    /**
     * HoaDon constructor.
     * @param $userHuy
     * @param $soHoaDon
     * @param $ngayTaoHoaDon
     * @param $noiDung
     * @param $maLoaiHoaDon
     * @param $hoaDonId
     * @param $ngayNhapVien
     * @param $ngayXuatHoaDon
     * @param $maEinvoice
     * @param $isSuDungBangKe
     * @param $isGiuLai
     */
    public function __construct($userHuy = '', $soHoaDon = '', $ngayTaoHoaDon = '', $noiDung = '',
                                $maLoaiHoaDon = '', $hoaDonId = '', $ngayNhapVien = '', $ngayXuatHoaDon = '',
                                $maEinvoice = '', $isSuDungBangKe = '', $isGiuLai = '')
    {
        $this->userHuy = $userHuy;
        $this->soHoaDon = $soHoaDon;
        $this->ngayTaoHoaDon = $ngayTaoHoaDon;
        $this->noiDung = $noiDung;
        $this->maLoaiHoaDon = $maLoaiHoaDon;
        $this->hoaDonId = $hoaDonId;
        $this->ngayNhapVien = $ngayNhapVien;
        $this->ngayXuatHoaDon = $ngayXuatHoaDon;
        $this->maEinvoice = $maEinvoice;
        $this->isSuDungBangKe = $isSuDungBangKe;
        $this->isGiuLai = $isGiuLai;
    }


    /**
     * @return mixed
     */
    public function getUserHuy()
    {
        return $this->userHuy;
    }

    /**
     * @param mixed $userHuy
     */
    public function setUserHuy($userHuy)
    {
        $this->userHuy = $userHuy;
    }

    /**
     * @return mixed
     */
    public function getSoHoaDon()
    {
        return $this->soHoaDon;
    }

    /**
     * @param mixed $soHoaDon
     */
    public function setSoHoaDon($soHoaDon)
    {
        $this->soHoaDon = $soHoaDon;
    }

    /**
     * @return mixed
     */
    public function getNgayTaoHoaDon()
    {
        return $this->ngayTaoHoaDon;
    }

    /**
     * @param mixed $ngayTaoHoaDon
     */
    public function setNgayTaoHoaDon($ngayTaoHoaDon)
    {
        $this->ngayTaoHoaDon = $ngayTaoHoaDon;
    }

    /**
     * @return mixed
     */
    public function getNoiDung()
    {
        return $this->noiDung;
    }

    /**
     * @param mixed $noiDung
     */
    public function setNoiDung($noiDung)
    {
        $this->noiDung = $noiDung;
    }

    /**
     * @return mixed
     */
    public function getMaLoaiHoaDon()
    {
        return $this->maLoaiHoaDon;
    }

    /**
     * @param mixed $maLoaiHoaDon
     */
    public function setMaLoaiHoaDon($maLoaiHoaDon)
    {
        $this->maLoaiHoaDon = $maLoaiHoaDon;
    }

    /**
     * @return mixed
     */
    public function getHoaDonId()
    {
        return $this->hoaDonId;
    }

    /**
     * @param mixed $hoaDonId
     */
    public function setHoaDonId($hoaDonId)
    {
        $this->hoaDonId = $hoaDonId;
    }

    /**
     * @return mixed
     */
    public function getNgayNhapVien()
    {
        return $this->ngayNhapVien;
    }

    /**
     * @param mixed $ngayNhapVien
     */
    public function setNgayNhapVien($ngayNhapVien)
    {
        $this->ngayNhapVien = $ngayNhapVien;
    }

    /**
     * @return mixed
     */
    public function getNgayXuatHoaDon()
    {
        return $this->ngayXuatHoaDon;
    }

    /**
     * @param mixed $ngayXuatHoaDon
     */
    public function setNgayXuatHoaDon($ngayXuatHoaDon)
    {
        $this->ngayXuatHoaDon = $ngayXuatHoaDon;
    }

    /**
     * @return mixed
     */
    public function getMaEinvoice()
    {
        return $this->maEinvoice;
    }

    /**
     * @param mixed $maEinvoice
     */
    public function setMaEinvoice($maEinvoice)
    {
        $this->maEinvoice = $maEinvoice;
    }

    /**
     * @return mixed
     */
    public function getIsSuDungBangKe()
    {
        return $this->isSuDungBangKe;
    }

    /**
     * @param mixed $isSuDungBangKe
     */
    public function setIsSuDungBangKe($isSuDungBangKe)
    {
        $this->isSuDungBangKe = $isSuDungBangKe;
    }

    /**
     * @return mixed
     */
    public function getIsGiuLai()
    {
        return $this->isGiuLai;
    }

    /**
     * @param mixed $isGiuLai
     */
    public function setIsGiuLai($isGiuLai)
    {
        $this->isGiuLai = $isGiuLai;
    }

    /**
     * @param $dsachSP
     * @param $benBan
     * @param $benMua
     * @param $hinhThucThanhToan
     */
    public static function XuatHoaDonDienTu($dsachSP, $benBan, $benMua, $hinhThucThanhToan){
        //Body XuatHoaDonDienTu
        $content = self::contentEnvoices();
        $xmlBenBan = BenBan_Library::formatXml($benBan);
        $xmlBenMua = BenMua_Library::formatXml($benMua);
        $xmlHinhThucThanhToan = HinhThucThanhToan_Library::formatXml($hinhThucThanhToan);
        $xmlSanPham = SanPham_Library::formatXml($dsachSP);
        $xmlMoney = SanPham_Library::calculateMoney($dsachSP);
        //XML benBan
        if($xmlBenBan != ""){
            $content .= $xmlBenBan;
        }
        //XML benMua
        if($xmlBenMua != ""){
            $content .= $xmlBenMua;
        }
        //XML hinhThucThanhToan
        if($xmlHinhThucThanhToan != ""){
            $content .= $xmlHinhThucThanhToan;
        }
        //XML TienThanhToan
        if($xmlMoney != ""){
            $content .= $xmlMoney;
        }
        if($xmlSanPham != "") {
            $content .= $xmlSanPham;
        }
        $body = Connect_Library::setBody(self::$KEY_XML_FUNCTION_XUAT_HOA_DON_DIEN_TU, self::startXuatHoaDonXml.$content.self::endXuatHoaDonXml);
        $result = Connect_Library::callApi($body);

        $objectResult = self::buildObjectFromXml($result);

        if(count($objectResult['error']) > 0){
            return null;
        }else{
            unset($objectResult['error']);
            return $objectResult;
        }
    }

    /**
     * @return false|string
     */
    public static function getNgayHienTai(){
        return date("Y-m-d");
    }

    /**
     * @return string
     */
    public static function contentEnvoices(){
        return $content = '<inv:TenLoaiHoaDon>'.self::TenLoaiHoaDon.'</inv:TenLoaiHoaDon>
                    <inv:MauSo>'.self::MauSo.'</inv:MauSo>
                    <inv:KyHieu>'.self::KyHieu.'</inv:KyHieu>
                    <inv:SoHoaDon/>
                    <inv:NgayTaoHoaDon>'.self::getNgayHienTai().'</inv:NgayTaoHoaDon>
                    <inv:NgayXuatHoaDon>'.self::getNgayHienTai().'</inv:NgayXuatHoaDon>';
    }

    /**
     * @param $xml
     * @return array|bool
     */
    public static function buildObjectFromXml($xml){
        $arrayResult = (json_decode(json_encode(simplexml_load_string(strtr($xml, array(' xmlns:'=>' ')))), 1));

        if(isset($arrayResult['soap:Body'])){
            $arrayBody = $arrayResult['soap:Body'];
            $resultArray = $arrayBody[self::$KEY_XML_RESPONSE_XUAT_HOA_DON_DIEN_TU][self::$KEY_XML_RESULT_XUAT_HOA_DON_DIEN_TU];

            //Return Error
            $error = $resultArray['MsgError'];

            //Return Object Hoa Don
            $hoaDon = self::buildObjectFromArray($resultArray);
            //Return Object BenBan_Library
            $benBan = BenBan_Library::buildObjectFromArray($resultArray);

            //Return Object BenMua_Library
            $benMua = BenMua_Library::buildObjectFromArray($resultArray);

            //Return Object SanPham_Library
            $arraySanPham = SanPham_Library::buildObjectFromArray($resultArray);

            return [
                'hoadon' => $hoaDon,
                'benmua' => $benMua,
                'benban' => $benBan,
                'sanpham' => $arraySanPham,
                'error' => $error
            ];
        }

        return false;
    }

    /**
     * @param $xml
     * @return array
     */
    public static function buildObjectHuyHoaDonFromXml($xml){
        $arrayResult = (json_decode(json_encode(simplexml_load_string(strtr($xml, array(' xmlns:'=>' ')))), 1));
        if(isset($arrayResult['soap:Body'])){
            $arrayBody = $arrayResult['soap:Body'];
            $resultArray = $arrayBody[self::$KEY_XML_RESPONSE_HUY_HOA_DON_DIEN_TU][self::$KEY_XML_RESULT_HUY_HOA_DON_DIEN_TU];
            //Return Error
            $error = $resultArray['MsgError'];
            if(count($error) > 0 ){
                return [
                    'status' => false,
                    'error'  => $error
                ];
            }else{
                return ['status' => true];
            }
        }
    }

    /**
     * @param $hoaDon
     * @return string
     */
    public static function formatXml($hoaDon){
        $xml = "";
        if($hoaDon instanceof HoaDon_Library){
            $xml = '<inv:MA_SO_THUE>0304680974</inv:MA_SO_THUE>
                    <inv:MauSo>'.self::MauSo.'</inv:MauSo>
                    <inv:KyHieu>'.self::KyHieu.'</inv:KyHieu>
                    <inv:UserHuy>'.$hoaDon->getUserHuy().'</inv:UserHuy>
                    <inv:SoHoaDon>'.$hoaDon->getSoHoaDon().'</inv:SoHoaDon>
                    <inv:NgayTaoHoaDon>'.$hoaDon->getNgayTaoHoaDon().'</inv:NgayTaoHoaDon>
                    <inv:NoiDung>'.$hoaDon->getNoiDung().'</inv:NoiDung>';
        }
        return $xml;
    }

    /**
     * @param $hoaDon
     * @return bool
     */
    public static function HuyHoaDon($hoaDon){
        $content = self::formatXml($hoaDon);
        $body = Connect_Library::setBody(self::$KEY_XML_FUNCTION_HUY_HOA_DON, self::startHuyHoaDonXml.$content.self::endHuyHoaDonXml);
        $result = Connect_Library::callApi($body);

        $objectResult = self::buildObjectHuyHoaDonFromXml($result);

        if(!$objectResult['status']){
            return false;
        }else{
            return true;
        }
    }

    /**
     * @param $resultArray
     * @return HoaDon_Library
     */
    public static function buildObjectFromArray($resultArray){
        $hoaDon = new HoaDon_Library();
        $hoaDon->setMaLoaiHoaDon($resultArray['MaLoaiHoaDon']);
        $hoaDon->setHoaDonId($resultArray['dhoadonid']);
        $hoaDon->setSoHoaDon($resultArray['SoHoaDon']);
        $hoaDon->setNgayNhapVien($resultArray['NgayNhapVien']);
        $hoaDon->setNgayTaoHoaDon($resultArray['NgayTaoHoaDon']);
        $hoaDon->setNgayXuatHoaDon($resultArray['NgayXuatHoaDon']);
        $hoaDon->setMaEinvoice($resultArray['MaEinvoice']);
        $hoaDon->setIsGiuLai($resultArray['IsGiuLai']);
        $hoaDon->setIsSuDungBangKe($resultArray['isSuDungBangKe']);
        return $hoaDon;
    }

    /**
     * @param $hoaDonGoc
     * @param $dsachSP
     * @param $benBan
     * @param $benMua
     * @param $hinhThucThanhToan
     * @return array|bool|null
     */
    public static function UpdateHoaDon($hoaDonGoc, $dsachSP, $benBan, $benMua, $hinhThucThanhToan){
        //Create new envoice
        $objectResult = self::XuatHoaDonDienTu($dsachSP, $benBan, $benMua, $hinhThucThanhToan);
        //Cancel old envoice
        if(count($objectResult['error']) > 0){
            return null;
        }else{
            $objresult = self::HuyHoaDon($hoaDonGoc);
            if(!$objresult['status']){
                return null;
            }else{
                return $objectResult;
            }
        }
    }

    /**
     * @param $hoaDon
     * @return mixed|null|string
     */
    public static function ViewHoaDon($hoaDon){
        if($hoaDon instanceof HoaDon_Library){
            if($hoaDon->getMaEinvoice()){
                $link =  self::linkViewHoaDon.$hoaDon->getMaEinvoice();
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $link);
                curl_setopt($ch, CURLOPT_HEADER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                $url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
                //Ma Envoices khong chinh xac
                if (strpos($url, self::linkViewHoaDon) !== false) {
                    return null;
                }
                $err = curl_error($ch);
                curl_close($ch);
                if ($err) {
                    return "cURL Error #:" . $err;
                } else {
                    return $url;
                }
            }else{
                return null;
            }
        }
    }

    /**
     * @param $linkViewHoaDon
     * @return bool|null|string
     */
    public static function DownloadHoaDon($linkViewHoaDon){
        if($linkViewHoaDon == null){
            return null;
        }else{
            $rootDir = realpath($_SERVER["DOCUMENT_ROOT"]);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $linkViewHoaDon);
            //Create a new file where you want to save
            $fp = fopen(basename($rootDir.'/'.'hoadon_'.date("m_d_Y_H_i_s").'.pdf'), 'w');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_exec ($ch);
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);
            fclose($fp);
            if ($err) {
                return "cURL Error #:" . $err;
            } else {
                return true;
            }

        }
    }
}