<?php if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class SanPham_Library{
    public $soThuTu;
    public $maHang;
    public $tenHang;
    public $donViTinh;
    public $soLuong;
    public $donGia;
    public $thanhTien;
    public $vat;
    public $tienVat;

    const DongTienThanhToan = 'VND';
    const TrangThaiDieuChinh = '1';

    /**
     * SanPham constructor.
     * @param $soThuTu
     * @param $maHang
     * @param $tenHang
     * @param $donViTinh
     * @param $soLuong
     * @param $donGia
     * @param $thanhTien
     * @param $vat
     * @param $tienVat
     */
    public function __construct($soThuTu = '', $maHang = '', $tenHang = '', $donViTinh = '',
                                $soLuong = '', $donGia = '', $thanhTien = '', $vat = '', $tienVat = '')
    {
        $this->soThuTu = $soThuTu;
        $this->maHang = $maHang;
        $this->tenHang = $tenHang;
        $this->donViTinh = $donViTinh;
        $this->soLuong = $soLuong;
        $this->donGia = $donGia;
        $this->thanhTien = $thanhTien;
        $this->vat = $vat;
        $this->tienVat = $tienVat;
    }

    /**
     * @return mixed
     */
    public function getSoThuTu()
    {
        return $this->soThuTu;
    }

    /**
     * @param mixed $soThuTu
     */
    public function setSoThuTu($soThuTu)
    {
        $this->soThuTu = $soThuTu;
    }

    /**
     * @return mixed
     */
    public function getMaHang()
    {
        return $this->maHang;
    }

    /**
     * @param mixed $maHang
     */
    public function setMaHang($maHang)
    {
        $this->maHang = $maHang;
    }

    /**
     * @return mixed
     */
    public function getTenHang()
    {
        return $this->tenHang;
    }

    /**
     * @param mixed $tenHang
     */
    public function setTenHang($tenHang)
    {
        $this->tenHang = $tenHang;
    }

    /**
     * @return mixed
     */
    public function getDonViTinh()
    {
        return $this->donViTinh;
    }

    /**
     * @param mixed $donViTinh
     */
    public function setDonViTinh($donViTinh)
    {
        $this->donViTinh = $donViTinh;
    }

    /**
     * @return mixed
     */
    public function getSoLuong()
    {
        return $this->soLuong;
    }

    /**
     * @param mixed $soLuong
     */
    public function setSoLuong($soLuong)
    {
        $this->soLuong = $soLuong;
    }

    /**
     * @return mixed
     */
    public function getDonGia()
    {
        return $this->donGia;
    }

    /**
     * @param mixed $donGia
     */
    public function setDonGia($donGia)
    {
        $this->donGia = $donGia;
    }

    /**
     * @return mixed
     */
    public function getThanhTien()
    {
        return $this->thanhTien;
    }

    /**
     * @param mixed $thanhTien
     */
    public function setThanhTien($thanhTien)
    {
        $this->thanhTien = $thanhTien;
    }

    /**
     * @return mixed
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param mixed $vat
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    }

    /**
     * @return mixed
     */
    public function getTienVat()
    {
        return $this->tienVat;
    }

    /**
     * @param mixed $tienVat
     */
    public function setTienVat($tienVat)
    {
        $this->tienVat = $tienVat;
    }

    /**
     * @param $dsachSP
     * @return string
     */
    public static function formatXml($dsachSP){
        $contentSanPham = '';
        if(is_array($dsachSP) && count($dsachSP) > 0){
            $contentSanPham .= '<inv:HangHoas>';
            foreach ($dsachSP as $key => $sp){
                $contentSanPham .= '<inv:HangHoaEntity>
                                    <inv:SoThuTu>'.$sp->getSoThuTu().'</inv:SoThuTu>
                                    <inv:MaHang>'.$sp->getMaHang().'</inv:MaHang>
                                    <inv:TenHang>'.$sp->getTenHang().'</inv:TenHang>
                                    <inv:DonViTinh>'.$sp->getDonViTinh().'</inv:DonViTinh>
                                    <inv:SoLuong>'.$sp->getSoLuong().'</inv:SoLuong>
                                    <inv:DonGia>'.$sp->getDonGia().'</inv:DonGia>
                                    <inv:ThanhTien>'.$sp->getThanhTien().'</inv:ThanhTien>
                                    <inv:Vat>'.$sp->getVat().'</inv:Vat>
                                    <inv:TienVat>'.$sp->getTienVat().'</inv:TienVat>
                                </inv:HangHoaEntity>';
            }
            $contentSanPham.= ' </inv:HangHoas>';
        }
        return $contentSanPham;
    }

    /**
     * @param $dsachSP
     * @return string
     */
    public static function calculateMoney($dsachSP){
        $content = '';
        if(is_array($dsachSP) && count($dsachSP) > 0){
            $tienThueVat = $tongTienHang = $tongTienThanhToan = $tongTienThanhToanBangChu = $dongTienThanhToan = $trangThaiDieuChinh = 0;
            foreach ($dsachSP as $key => $sp){
                $sum = ($sp->getSoLuong() * (float)$sp->getDonGia());
                $tienThueVat += (float)($sum * $sp->getVat() / 100);
                $tongTienHang += (float)$sum;
                $tongTienThanhToan += (float)($sum+$tienThueVat);
                $tongTienThanhToanBangChu = Helper_Library::convert_number_to_words($tongTienThanhToan);
            }
            $content = '<inv:TienThueVat>'.$tienThueVat.'</inv:TienThueVat>
                <inv:TongTienHang>'.$tongTienHang.'</inv:TongTienHang>
                <inv:TongTienThanhToan>'.$tongTienThanhToan.'</inv:TongTienThanhToan>
                <inv:TongTienThanhToanBangChu>'.$tongTienThanhToanBangChu.'</inv:TongTienThanhToanBangChu>
                <inv:DongTienThanhToan>'.self::DongTienThanhToan.'</inv:DongTienThanhToan>
                <inv:TrangThaiDieuChinh>'.self::TrangThaiDieuChinh.'</inv:TrangThaiDieuChinh>';
        }
        return $content;
    }

    /**
     * @param $resultArray
     * @return array
     */
    public static function buildObjectFromArray($resultArray){
        $arraySanPham = [];
        if(isset($resultArray['HangHoas'])){
            if(count($resultArray['HangHoas']) > 0 ){
                foreach ($resultArray['HangHoas'] as $sp){
                    $sanPham = new SanPham_Library();
                    $sanPham->setSoThuTu($sp['SoThuTu']);
                    $sanPham->setTenHang($sp['TenHang']);
                    $sanPham->setDonViTinh($sp['DonViTinh']);
                    $sanPham->setSoLuong($sp['SoLuong']);
                    $sanPham->setDonGia($sp['DonGia']);
                    $sanPham->setThanhTien($sp['ThanhTien']);
                    $sanPham->setVat($sp['Vat']);
                    $sanPham->setTienVat($sp['TienVat']);
                    $arraySanPham[] = $sanPham;
                }
            }
        }
        return $arraySanPham;
    }
}