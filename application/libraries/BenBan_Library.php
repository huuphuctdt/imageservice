<?php if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class BenBan_Library{
    protected $benBanMaDonVi;
    protected $benBanMaSoThue;
    protected $benBanTenDonVi;
    protected $benBanDiaChi;
    protected $benBanDienThoai;
    protected $benBanFax;
    protected $benBanTaiKhoanNganHang;
    protected $benBanTenNganHang;

    /**
     * BenBan constructor.
     * @param $benBanMaDonVi
     * @param $benBanMaSoThue
     * @param $benBanTenDonVi
     * @param $benBanDiaChi
     * @param $benBanDienThoai
     * @param $benBanFax
     * @param $benBanTaiKhoanNganHang
     * @param $benBanTenNganHang
     */
    public function __construct($benBanMaDonVi = '', $benBanMaSoThue = '', $benBanTenDonVi = '',
                                $benBanDiaChi = '', $benBanDienThoai = '', $benBanFax = '',
                                $benBanTaiKhoanNganHang = '', $benBanTenNganHang = '')
    {
        $this->benBanMaDonVi = $benBanMaDonVi;
        $this->benBanMaSoThue = $benBanMaSoThue;
        $this->benBanTenDonVi = $benBanTenDonVi;
        $this->benBanDiaChi = $benBanDiaChi;
        $this->benBanDienThoai = $benBanDienThoai;
        $this->benBanFax = $benBanFax;
        $this->benBanTaiKhoanNganHang = $benBanTaiKhoanNganHang;
        $this->benBanTenNganHang = $benBanTenNganHang;
    }


    /**
     * @return mixed
     */
    public function getBenBanMaDonVi()
    {
        return $this->benBanMaDonVi;
    }

    /**
     * @param mixed $benBanMaDonVi
     */
    public function setBenBanMaDonVi($benBanMaDonVi)
    {
        $this->benBanMaDonVi = $benBanMaDonVi;
    }

    /**
     * @return mixed
     */
    public function getBenBanMaSoThue()
    {
        return $this->benBanMaSoThue;
    }

    /**
     * @param mixed $benBanMaSoThue
     */
    public function setBenBanMaSoThue($benBanMaSoThue)
    {
        $this->benBanMaSoThue = $benBanMaSoThue;
    }

    /**
     * @return mixed
     */
    public function getBenBanTenDonVi()
    {
        return $this->benBanTenDonVi;
    }

    /**
     * @param mixed $benBanTenDonVi
     */
    public function setBenBanTenDonVi($benBanTenDonVi)
    {
        $this->benBanTenDonVi = $benBanTenDonVi;
    }

    /**
     * @return mixed
     */
    public function getBenBanDiaChi()
    {
        return $this->benBanDiaChi;
    }

    /**
     * @param mixed $benBanDiaChi
     */
    public function setBenBanDiaChi($benBanDiaChi)
    {
        $this->benBanDiaChi = $benBanDiaChi;
    }

    /**
     * @return mixed
     */
    public function getBenBanDienThoai()
    {
        return $this->benBanDienThoai;
    }

    /**
     * @param mixed $benBanDienThoai
     */
    public function setBenBanDienThoai($benBanDienThoai)
    {
        $this->benBanDienThoai = $benBanDienThoai;
    }

    /**
     * @return mixed
     */
    public function getBenBanFax()
    {
        return $this->benBanFax;
    }

    /**
     * @param mixed $benBanFax
     */
    public function setBenBanFax($benBanFax)
    {
        $this->benBanFax = $benBanFax;
    }

    /**
     * @return mixed
     */
    public function getBenBanTaiKhoanNganHang()
    {
        return $this->benBanTaiKhoanNganHang;
    }

    /**
     * @param mixed $benBanTaiKhoanNganHang
     */
    public function setBenBanTaiKhoanNganHang($benBanTaiKhoanNganHang)
    {
        $this->benBanTaiKhoanNganHang = $benBanTaiKhoanNganHang;
    }

    /**
     * @return mixed
     */
    public function getBenBanTenNganHang()
    {
        return $this->benBanTenNganHang;
    }

    /**
     * @param mixed $benBanTenNganHang
     */
    public function setBenBanTenNganHang($benBanTenNganHang)
    {
        $this->benBanTenNganHang = $benBanTenNganHang;
    }

    /**
     * @param $benBan
     * @return string
     */
    public static function formatXml($benBan){
        $xml = "";
        if($benBan instanceof BenBan_Library){
            $xml = '<inv:BenBanMaDonVi>'.$benBan->getBenBanMaDonVi().'</inv:BenBanMaDonVi>
                    <inv:BenBanMaSoThue>'.$benBan->getBenBanMaSoThue().'</inv:BenBanMaSoThue>
                    <inv:BenBanTenDonVi>'.$benBan->getBenBanTenDonVi().'</inv:BenBanTenDonVi>
                    <inv:BenBanDiaChi>'.$benBan->getBenBanDiaChi().'</inv:BenBanDiaChi>';
        }
        return $xml;
    }

    /**
     * @param $resultArray
     * @return BenBan_Library
     */
    public static function buildObjectFromArray($resultArray){
        //Return Object BenBan
        $benBan = new BenBan_Library();
        $benBan->setBenBanMaSoThue($resultArray['BenBanMaSoThue']);
        $benBan->setBenBanTenDonVi($resultArray['BenBanTenDonVi']);
        $benBan->setBenBanDiaChi($resultArray['BenBanDiaChi']);
        $benBan->setBenBanDienThoai($resultArray['BenBanDienThoai']);
        $benBan->setBenBanFax($resultArray['BenBanFax']);
        $benBan->setBenBanTaiKhoanNganHang($resultArray['BenBanTaiKhoanNganHang']);
        $benBan->setBenBanTenNganHang($resultArray['BenBanTenNganHang']);
        return $benBan;
    }
}