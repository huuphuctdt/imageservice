<?php if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class BenMua_Library{
    protected $benMuaMaDonVi;
    protected $benMuaMaSoThue;
    protected $benMuaTenDonVi;
    protected $benMuaHoTen;
    protected $benMuaDiaChi;
    protected $benMuaDienThoai;
    protected $benMuaFax;
    protected $benMuaEmail;
    protected $benMuaTaiKhoanNganHang;
    protected $benMuaTenNganHang;

    /**
     * BenMua constructor.
     * @param $benMuaMaDonVi
     * @param $benMuaMaSoThue
     * @param $benMuaTenDonVi
     * @param $benMuaHoTen
     * @param $benMuaDiaChi
     * @param $benMuaDienThoai
     * @param $benMuaFax
     * @param $benMuaEmail
     * @param $benMuaTaiKhoanNganHang
     * @param $benMuaTenNganHang
     */
    public function __construct($benMuaMaDonVi = '', $benMuaMaSoThue = '', $benMuaTenDonVi = '',
                                $benMuaHoTen = '', $benMuaDiaChi = '', $benMuaDienThoai = '', $benMuaFax = '',
                                $benMuaEmail = '', $benMuaTaiKhoanNganHang = '', $benMuaTenNganHang = '')
    {
        $this->benMuaMaDonVi = $benMuaMaDonVi;
        $this->benMuaMaSoThue = $benMuaMaSoThue;
        $this->benMuaTenDonVi = $benMuaTenDonVi;
        $this->benMuaHoTen = $benMuaHoTen;
        $this->benMuaDiaChi = $benMuaDiaChi;
        $this->benMuaDienThoai = $benMuaDienThoai;
        $this->benMuaFax = $benMuaFax;
        $this->benMuaEmail = $benMuaEmail;
        $this->benMuaTaiKhoanNganHang = $benMuaTaiKhoanNganHang;
        $this->benMuaTenNganHang = $benMuaTenNganHang;
    }

    /**
     * @return mixed
     */
    public function getBenMuaMaDonVi()
    {
        return $this->benMuaMaDonVi;
    }

    /**
     * @param mixed $benMuaMaDonVi
     */
    public function setBenMuaMaDonVi($benMuaMaDonVi)
    {
        $this->benMuaMaDonVi = $benMuaMaDonVi;
    }

    /**
     * @return mixed
     */
    public function getBenMuaMaSoThue()
    {
        return $this->benMuaMaSoThue;
    }

    /**
     * @param mixed $benMuaMaSoThue
     */
    public function setBenMuaMaSoThue($benMuaMaSoThue)
    {
        $this->benMuaMaSoThue = $benMuaMaSoThue;
    }

    /**
     * @return mixed
     */
    public function getBenMuaTenDonVi()
    {
        return $this->benMuaTenDonVi;
    }

    /**
     * @param mixed $benMuaTenDonVi
     */
    public function setBenMuaTenDonVi($benMuaTenDonVi)
    {
        $this->benMuaTenDonVi = $benMuaTenDonVi;
    }

    /**
     * @return mixed
     */
    public function getBenMuaHoTen()
    {
        return $this->benMuaHoTen;
    }

    /**
     * @param mixed $benMuaHoTen
     */
    public function setBenMuaHoTen($benMuaHoTen)
    {
        $this->benMuaHoTen = $benMuaHoTen;
    }

    /**
     * @return mixed
     */
    public function getBenMuaDiaChi()
    {
        return $this->benMuaDiaChi;
    }

    /**
     * @param mixed $benMuaDiaChi
     */
    public function setBenMuaDiaChi($benMuaDiaChi)
    {
        $this->benMuaDiaChi = $benMuaDiaChi;
    }

    /**
     * @return mixed
     */
    public function getBenMuaDienThoai()
    {
        return $this->benMuaDienThoai;
    }

    /**
     * @param mixed $benMuaDienThoai
     */
    public function setBenMuaDienThoai($benMuaDienThoai)
    {
        $this->benMuaDienThoai = $benMuaDienThoai;
    }

    /**
     * @return mixed
     */
    public function getBenMuaFax()
    {
        return $this->benMuaFax;
    }

    /**
     * @param mixed $benMuaFax
     */
    public function setBenMuaFax($benMuaFax)
    {
        $this->benMuaFax = $benMuaFax;
    }

    /**
     * @return mixed
     */
    public function getBenMuaEmail()
    {
        return $this->benMuaEmail;
    }

    /**
     * @param mixed $benMuaEmail
     */
    public function setBenMuaEmail($benMuaEmail)
    {
        $this->benMuaEmail = $benMuaEmail;
    }

    /**
     * @return mixed
     */
    public function getBenMuaTaiKhoanNganHang()
    {
        return $this->benMuaTaiKhoanNganHang;
    }

    /**
     * @param mixed $benMuaTaiKhoanNganHang
     */
    public function setBenMuaTaiKhoanNganHang($benMuaTaiKhoanNganHang)
    {
        $this->benMuaTaiKhoanNganHang = $benMuaTaiKhoanNganHang;
    }

    /**
     * @return mixed
     */
    public function getBenMuaTenNganHang()
    {
        return $this->benMuaTenNganHang;
    }

    /**
     * @param mixed $benMuaTenNganHang
     */
    public function setBenMuaTenNganHang($benMuaTenNganHang)
    {
        $this->benMuaTenNganHang = $benMuaTenNganHang;
    }

    /**
     * @param $benMua
     * @return string
     */
    public static function formatXml($benMua){
        $xml = "";
        if($benMua instanceof BenMua_Library){
            $xml = '<inv:BenMuaMaDonVi>'.$benMua->getBenMuaMaDonVi().'</inv:BenMuaMaDonVi>
                    <inv:BenMuaMaSoThue>'.$benMua->getBenMuaMaSoThue().'</inv:BenMuaMaSoThue>
                    <inv:BenMuaTenDonVi>'.$benMua->getBenMuaTenDonVi().'</inv:BenMuaTenDonVi>
                    <inv:BenMuaHoTen>'.$benMua->getBenMuaHoTen().'</inv:BenMuaHoTen>
                    <inv:BenMuaDiaChi>'.$benMua->getBenMuaDiaChi().'</inv:BenMuaDiaChi>
                    <inv:BenMuaDienThoai>'.$benMua->getBenMuaDienThoai().'</inv:BenMuaDienThoai>
                    <inv:BenMuaFax>'.$benMua->getBenMuaFax().'</inv:BenMuaFax>
                    <inv:BenMuaEmail>'.$benMua->getBenMuaEmail().'</inv:BenMuaEmail>
                    <inv:BenMuaTaiKhoanNganHang>'.$benMua->getBenMuaTaiKhoanNganHang().'</inv:BenMuaTaiKhoanNganHang>
                    <inv:BenMuaTenNganHang>'.$benMua->getBenMuaTenNganHang().'</inv:BenMuaTenNganHang>';
        }
        return $xml;
    }

    /**
     * @param $resultArray
     * @return BenMua_Library
     */
    public static function buildObjectFromArray($resultArray){
        //Return Object BenMua
        $benMua = new BenMua_Library();
        $benMua->setBenMuaMaSoThue($resultArray['BenMuaMaSoThue']);
        $benMua->setBenMuaTenDonVi($resultArray['BenMuaTenDonVi']);
        $benMua->setBenMuaHoTen($resultArray['BenMuaHoTen']);
        $benMua->setBenMuaDiaChi($resultArray['BenMuaDiaChi']);
        $benMua->setBenMuaDienThoai($resultArray['BenMuaDienThoai']);
        $benMua->setBenMuaFax($resultArray['BenMuaFax']);
        $benMua->setBenMuaEmail($resultArray['BenMuaEmail']);
        $benMua->setBenMuaTaiKhoanNganHang($resultArray['BenMuaTaiKhoanNganHang']);
        $benMua->setBenMuaTenNganHang($resultArray['BenMuaTenNganHang']);
        return $benMua;
    }
}