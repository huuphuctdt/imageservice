<?php if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Connect_Library
{
    public static $userName = "tsd";

    public static $passWord = "tsd@123";

    public static $url = "http://210.245.8.58:6789/EinvoiceService.asmx";

    public static $port = "6789";

    const startXml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:inv="http://thaison.vn/inv">';
    const endXml = '</soapenv:Envelope>';

    /**
     * @return string
     */
    public static function setHeader(){
        $header = '<soapenv:Header>
                        <inv:Authentication>
                            <inv:userName>'.self::$userName.'</inv:userName>
                            <inv:password>'.self::$passWord.'</inv:password>
                        </inv:Authentication>
                    </soapenv:Header>';
        return $header;
    }

    /**
     * @param $functionName
     * @param $content
     * @return string
     */
    public static function setBody($functionName, $content){
        $body = '<soapenv:Body><inv:'.$functionName.'>'.$content.'</inv:'.$functionName.'></soapenv:Body>';
        return $body;
    }

    /**
     * @param $header
     * @param $content
     * @return mixed|string
     */
    public static function callApi($content){

        if($content && $content != ""){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_PORT => self::$port,
                CURLOPT_URL => self::$url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => self::startXml.self::setHeader().$content.self::endXml,
                CURLOPT_HTTPHEADER => array("Content-Type: text/xml"),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return "cURL Error #:" . $err;
            } else {
                return $response;
            }
        }
    }
}